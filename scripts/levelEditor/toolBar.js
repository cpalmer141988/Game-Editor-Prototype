var toolBar = levelEditor.toolBar = {};
toolBar.toolSet = "";
toolBar.selectedTool = "";
toolBar.toolConfig = {color:"black", allowUpdate:true};

toolBar.selectTool = function(target){
	var selected = this.selectedTool = (this.selectedTool != target) ? target : "";
	var toolSet = this.toolSet;
	this.toolConfig = toolBar.configTool(selected);

	var toolBarElem = $(toolSet);
	var children = toolBarElem.children;

	for(var i=0; i<children.length; i++)
		children[i].style.border = "";

	if(selected)
		$(selected).style.border = "3px solid black";
}

toolBar.configTool = function(tool){
	var config = {};
	switch(tool){
		case "ground":
			config.color = "brown";
			config.allowUpdate = true;
			return config;
		case "wall":
			config.color = "grey";
			config.allowUpdate = true;
			return config;
		case "player":
			config.color = "blue";
			config.width = 5;
			config.height = 5;
			config.allowUpdate = false;
			return config;
		case "enemy_1":
			config.color = "red";
			config.width = 5;
			config.height = 5;
			config.allowUpdate = false;
			return config;
		default:
			config.color = "black";
			config.allowUpdate = true;
			return config;
	}
}
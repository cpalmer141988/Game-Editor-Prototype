levelEditor.levelName = "";
levelEditor.scrollLeft = 0;
levelEditor.scrollTop = 0;
levelEditor.activePreview = "";
levelEditor.selectedBlock = "";
levelEditor.blockDrawUpdate = false;

levelEditor.scrollGrid = function(event){
	var scrollLeft = levelEditor.scrollLeft = event.currentTarget.scrollLeft;
	var scrollTop = levelEditor.scrollTop = event.currentTarget.scrollTop;

	var e_grid = $('e_grid');
	e_grid.scrollLeft = scrollLeft;
	e_grid.scrollTop = scrollTop;
}

levelEditor.add_BlockPreview = function(event){
	var toolInterface =  $('toolInterface');
	var levelEditor = window.levelEditor; //top.levelEditor; //This used to fucken work... then... no, for no reason it's top and levelEditor are undefined?
	var activePreview = levelEditor.activePreview;
	//Tool Bar variables
	var toolConfig = toolBar.toolConfig;
	
	var positionInfo = grid.getGridPosition(event);
	var gridX = positionInfo.x;
	var gridY = positionInfo.y;

	var width = toolConfig.width || 1;
	var height = toolConfig.height || 1;
	var left = (gridX - Math.floor(width/2));// * gridScale) + "px";
	var top = (gridY - (height-1));// * gridScale) + "px";
	width = width;// * gridScale + "px";
	height = height;//* gridScale + "px";
	
	var id = "preview"+(left*gridScale+"px")+","+(top*gridScale+"px")+","+(width*gridScale+"px")+","+(height*gridScale+"px"); 
	
	if(activePreview == id)
		return;
	else if(activePreview != "")
		levelEditor.remove_BlockPreview(activePreview); 

	//ADD BLOCK PREVIEW
	var block = document.createElement("div");
	block.id = id;

	/*var results = collision.detectCollision(id, left, top, width, height);
	if(results.collided)
		return; //you collided with another object!!!*/


	block.style.left = left * gridScale + "px";
	block.style.top = top * gridScale + "px";
	block.style.width = width * gridScale + "px";
	block.style.height = height * gridScale + "px";

	block.style.backgroundColor = toolConfig.color;
	block.style.opacity = 0.5;
	block.onclick = levelEditor.selectBlock;
	block.onmousemove = levelEditor.add_BlockPreview;

	//TESTING

	/*block.draggable="true";
	block.ondragstart= levelEditor.drag;*/

	//block.onmouseover = levelEditor.test;

	//END TESTING

	toolInterface.appendChild(block);
	levelEditor.activePreview = id;
}

//TESTING
levelEditor.test = function(event){ 
	//event.dataTransfer.setData("Text",event.target.id);
	var mousePosition = grid.getMousePosition(event);
	console.outputToConsole(mousePosition.x + "," + mousePosition.y + "; ");
}
//END TESTING

levelEditor.remove_BlockPreview = function(element){
	var element = $(element);
	if(element)
		element.parentNode.removeChild(element);
}

levelEditor.add_Block = function(event){
	var toolInterface =  $('toolInterface');
	var levelEditor = window.levelEditor; //top.levelEditor; //This used to fucken work... then... no, for no reason top is undefined?
	var selectedBlock = levelEditor.selectedBlock;
	//Tool Bar variables
	var selectedTool = toolBar.selectedTool;
	var toolConfig = toolBar.toolConfig;
	var allowUpdate = toolConfig.allowUpdate;

	var positionInfo = grid.getGridPosition(event);
	var gridX = positionInfo.x;
	var gridY = positionInfo.y;

	var width = toolConfig.width || 1;
	var height = toolConfig.height || 1;
	var left = gridX - Math.floor(width/2);
	var top = gridY - (height-1);

	//PREVENT CLICKING INSIDE AN ALREADY DRAWN BLOCK
	if(levelEditor.blockDrawUpdate == true){
		levelEditor.blockDrawUpdate = false;
		return;
	}

	//CAN'T ADD A BLOCK IF ONE IS SELECTED
	if(selectedBlock != "" && allowUpdate){
		levelEditor.update_Block(selectedBlock,gridX,gridY);
		return;
	}
	
	//ADD THE BLOCK
	var block = document.createElement("div");
	var id = block.id = selectedTool+","+(left*gridScale+"px")+","+(top*gridScale+"px")+","+(width*gridScale+"px")+","+(height*gridScale+"px");
	
	var results = collision.detectCollision(id, left, top, width, height);
	if(results.collided)
		return; //you collided with another object!!!

	block.style.left = left * gridScale + "px";
	block.style.top = top * gridScale + "px";
	block.style.width = width * gridScale + "px";
	block.style.height = height * gridScale + "px";
	
	block.style.backgroundColor = toolConfig.color;
	block.onclick = levelEditor.selectBlock;

	toolInterface.appendChild(block);
	grid.addObj(id);

	//timer.addTimer(levelEditor, "highlightBlock", 1000, args=[block.id,true]); //Timers are firing too fast, WTF!!!
	if(allowUpdate){
		levelEditor.selectedBlock = id;
		levelEditor.highlight_Block(block.id, true);
	}
}

levelEditor.highlight_Block = function(element,state){
	var block = $(element);
	block.style.outline = state ? "white solid thin" : "";
}

levelEditor.update_Block = function(element,x,y){
	var levelEditor = window.levelEditor;
	var gridScale = window.shell.gridScale;
	
	var block = $(element);
	var id = block.id;
	var oldX = parseInt(block.style.left) / gridScale;
	var oldY = parseInt(block.style.top) / gridScale;
	var oldW = parseInt(block.style.width) / gridScale;
	var oldH = parseInt(block.style.height) / gridScale;

	var console = $('console');
	//Shrinking Block Size Not Allowed!!!
	if(oldW > 1){
		if( (oldX<x) && (x-oldX+1 < oldW) ){
			console.value = "Shrinking block width NOT ALLOWED!!! \r\n" + console.value;
			return;
		}
	}
	if(oldH > 1){
		if( (oldY<y) && (y-oldY+1 < oldH) ){
			console.value = "Shrinking block height NOT ALLOWED!!! \r\n" + console.value;
			return;
		}
	}

	//Blocks can't be larger than 100 x 100!!!
	if( (oldX - x) > 99 || (x - oldX) > 99 ){
		console.value = "Maximum block width is 100. \r\n" + console.value;
		return;
	}
	if( (oldY - y) > 99 || (y - oldY) > 99 ){
		console.value = "Maximum block height is 100. \r\n" + console.value;
		return;
	}

	/*---------------------------------*/
	/*-----8 DIRECTIONS OF DRAWING-----*/
	/*---------------------------------*/
	//Draw N
	if(oldX==x && oldY>y){
		var left = oldX;
		var top = y;
		var width = oldW;
		var height = oldY-y+oldH;

		var results = collision.detectCollision(block.id, left, top, width, height);
		if(results.collided)
			return; //you collided with another object!!!

		left = block.style.left = left*gridScale+"px";
		top = block.style.top = top*gridScale+"px"; 
		width = block.style.width = width*gridScale+"px";
		height = block.style.height = height*gridScale+"px";
	}
	//Draw NE
	else if(oldX<x && oldY>y){
		var left = oldX;
		var top = y;
		var width = x-oldX+1;
		var height = oldY-y+oldH;
		
		var results = collision.detectCollision(block.id, left, top, width, height);
		if(results.collided)
			return; //you collided with another object!!!

		left = block.style.left = left*gridScale+"px";
		top = block.style.top = top*gridScale+"px";
		width = block.style.width = width*gridScale+"px";
		height = block.style.height = height*gridScale+"px";
	}
	//Draw E
	else if(oldX<x && oldY==y){
		var left = oldX;
		var top = oldY;
		var width = x-oldX+1;
		var height = oldH;
		
		var results = collision.detectCollision(block.id, left, top, width, height);
		if(results.collided)
			return; //you collided with another object!!!

		left = block.style.left = left*gridScale+"px";
		top = block.style.top = top*gridScale+"px";
		width = block.style.width = width*gridScale+"px";
		height = block.style.height = height*gridScale+"px";
	}
	//Draw SE
	else if(oldX<x && oldY<y){
		var left = oldX;
		var top = oldY;
		var width = x-oldX+1;
		var height = y-oldY+1;
		
		var results = collision.detectCollision(block.id, left, top, width, height);
		if(results.collided)
			return; //you collided with another object!!!

		left = block.style.left = left*gridScale+"px";
		top = block.style.top = top*gridScale+"px";
		width = block.style.width = width*gridScale+"px";
		height = block.style.height = height*gridScale+"px";
	}
	//Draw S
	else if(oldX==x && oldY<y){
		var left = oldX;
		var top = oldY;
		var width = oldW;
		var height = y-oldY+1;
		
		var results = collision.detectCollision(block.id, left, top, width, height);
		if(results.collided)
			return; //you collided with another object!!!

		left = block.style.left = left*gridScale+"px";
		top = block.style.top = top*gridScale+"px";
		width = block.style.width = width*gridScale+"px";
		height = block.style.height = height*gridScale+"px";
	}
	//Draw SW
	else if(oldX>x && oldY<y){
		var left = x;
		var top = oldY;
		var width = oldX-x+oldW;
		var height = y-oldY+1;
		
		var results = collision.detectCollision(block.id, left, top, width, height);
		if(results.collided)
			return; //you collided with another object!!!

		left = block.style.left = left*gridScale+"px";
		top = block.style.top = top*gridScale+"px";
		width = block.style.width = width*gridScale+"px";
		height = block.style.height = height*gridScale+"px";
	}
	//Draw W
	else if(oldX>x && oldY==y){
		var left = x;
		var top = oldY;
		var width = oldX-x+oldW;
		var height = oldH;
		
		var results = collision.detectCollision(block.id, left, top, width, height);
		if(results.collided)
			return; //you collided with another object!!!

		left = block.style.left = left*gridScale+"px";
		top = block.style.top = top*gridScale+"px";
		width = block.style.width = width*gridScale+"px";
		height = block.style.height = height*gridScale+"px";
	}
	//Draw NW
	else if(oldX>x && oldY>y){
		var left = x;
		var top = y;
		var width = oldX-x+oldW;
		var height = oldY-y+oldH;
		
		var results = collision.detectCollision(block.id, left, top, width, height);
		if(results.collided)
			return; //you collided with another object!!!

		left = block.style.left = left*gridScale+"px";
		top = block.style.top = top*gridScale+"px";
		width = block.style.width = width*gridScale+"px";
		height = block.style.height = height*gridScale+"px";
	}

	block.id = id = id.slice(0,id.indexOf(",")) + ","+left+","+top+","+width+","+height;
	levelEditor.selectedBlock = id;
	grid.updateObj(id);
}

levelEditor.selectBlock = function(event){
	var levelEditor = top.levelEditor;
	var selectedBlock = levelEditor.selectedBlock;
	
	var positionInfo = grid.getGridPosition(event);
	var clickTarget = grid.getGridData(positionInfo.x, positionInfo.y); //need to get data on x and y

	if(clickTarget != "")
		levelEditor.blockDrawUpdate = true;
	if(selectedBlock == clickTarget || selectedBlock != "")
		return;
	
	levelEditor.selectedBlock = clickTarget;
	levelEditor.highlight_Block(clickTarget, true);
}
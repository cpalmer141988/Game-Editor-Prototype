var menuBar = levelEditor.menuBar = {};

menuBar.genGrid = function(x, y, lastX, lastY, drawCount, recallTime){
	var e_grid = $('e_grid');
	var toolInterface = $('toolInterface');
	var widthInput = x || +$('gridWidthTextField').value;//parseInt($('gridWidthTextField').value);
	if (widthInput === NaN){// || (widthInput % 2) != 0){
		alert('Grid Width must be an EVEN NUMBER');
		return;
	}
	var heightInput = y || +$('gridHeightTextField').value;//parseInt($('gridHeightTextField').value);
	if (heightInput === NaN){// || (heightInput % 2) != 0){
		alert('Grid Width must be an EVEN NUMBER');
		return;
	}

	var gridScale = shell.gridScale;
	var x = widthInput;// / gridScale;
	var y = heightInput;// / gridScale;

	if (lastX==undefined || lastY==undefined){
		levelEditor.grid = [];
		e_grid.innerHTML = "";
		toolInterface.innerHTML = "";
		loader.startLoadingProgress(x+y);
	}

	var lastX = lastX || 0;
	var lastY = lastY || 0;
	var currX = lastX + 0;
	var currY = lastY + 0;
	
	var drawCount = drawCount || 1000;
	var recallTime = recallTime || 1;
	
	for (currX; currX<x; currX++){
		loader.updateLoadingProgress();

		var xBlock = document.createElement("div");
		xBlock.style.width = (gridScale) + "px";
		xBlock.style.height = (gridScale*y) + "px";
		xBlock.style.left = currX * gridScale + "px";
		xBlock.style.top = "0px";

		xBlock.style.borderLeft = "1px solid black";
		xBlock.style.borderRight = "1px solid black";

		e_grid.appendChild(xBlock);
		drawCount--;

		if (drawCount==0){
			//currX++;
			timer.addTimer(menuBar, "genGrid", recallTime, args=[x,y,currX++,currY]);
			return;
		}
	}
	for (currY; currY<y; currY++){
		loader.updateLoadingProgress();

		var yBlock = document.createElement("div");
		yBlock.style.width = (gridScale*x) + "px";
		yBlock.style.height = (gridScale) + "px";
		yBlock.style.left = "0px";
		yBlock.style.top = currY * gridScale + "px";

		//yBlock.style.backgroundColor = "white";
		yBlock.style.borderTop = "1px solid black";
		yBlock.style.borderBottom = "1px solid black";
		e_grid.appendChild(yBlock);
		drawCount--;

		if (drawCount==0){
			//currY++;
			timer.addTimer(menuBar, "genGrid", recallTime, args=[x,y,currX,currY++]);
			return;
		}
	}

	grid.generateGrid(x,y); //levelEditor.genLevelGrid(x,y);
	toolInterface.style.width = (x * gridScale) + "px";
	toolInterface.style.height = (y * gridScale) + "px";
}

menuBar.clear = function(){
	levelEditor.selectedTool = "none";
	levelEditor.levelName = "";
	levelEditor.grid = [];
	levelEditor.selectedBlock = "";

	$('e_grid').innerHTML = "";
	var toolInterface = $('toolInterface');
	toolInterface.innerHTML = "";
	toolInterface.style.width = "0px";
	toolInterface.style.height = "0px";
	$('gridWidthTextField').value = "";
	$('gridHeightTextField').value = "";
}

menuBar.selectGameType = function(){
	var gameType = ($('gameType').value)+"_tools";
	var toolBarElem = $('e_toolBar');
	var children = toolBarElem.children;

	for(var i=0; i<children.length; i++) {
		var child = children[i];
		if (child.id != "toolGroups") {
			children[i].style.visibility = "hidden";
		}
	}

	if(gameType){
		$(gameType).style.visibility = "visible";
		toolBar.toolSet = gameType;
		toolBar.selectTool("");
	}
}

menuBar.selectSkyType = function(){
	var skyType = $('skyType');
}

menuBar.playTest = function(){
	var shell = $('shell');
	var width = parseInt(shell.style.width) + 100;
	var height = parseInt(shell.style.height) + 100;
	window.open("index.html",null,"toolbar=0,width="+width+",height="+height);
}

menuBar.loadLevel = function(){

}

menuBar.printToConsole = function(lastX, lastY, drawCount, recallTime){
	var console = $('console');
	var grid = window.levelEditor.grid;

	var lastX = lastX || 0;
	var lastY = lastY || 0;
	var currX = lastX + 0;
	var currY = lastY + 0;
	
	var drawCount = drawCount || 1000;
	var recallTime = recallTime || 1;

	if(lastX == 0 && lastY ==0)
		console.value = "";

	for (var x=currX; x<grid.length; x++){
		for (var y=currY; y<grid[x].length; y++){
			console.value += x+"x"+y+":"+grid[x][y]+";";
			drawCount--;

			if (drawCount==0){
				timer.addTimer(levelEditor, "printToConsole", recallTime, args=[x,y]);
				return;
			}
		}
	}
	//this times out if the grid is too large, this will likely happen to 
}
var console = levelEditor.console = {};

//This may be better off elsewhere
console.positionInfo = function(event){
	//var console = $('console');
	var c_mouseX = $('c_mouseX');
	var c_mouseY = $('c_mouseY');
	var c_gridX = $('c_gridX');
	var c_gridY = $('c_gridY');

	var mPos = grid.getMousePosition(event);
	var gPos = grid.getGridPosition(event);
	
	c_mouseX.innerHTML = mPos.x;
	c_mouseY.innerHTML = mPos.y;
	c_gridX.innerHTML = gPos.x;
	c_gridY.innerHTML = gPos.y;
}

console.outputToConsole = function(output){
	var console = $('console');
	console.value += output;
}

console.clearConsole = function(){
	var console = $('console');
	console.value = "";
}
var collision = shell.collision = {};

collision.detectCollision = function(element, x, y, w, h){
	var results = {};
	var collided = results.collided = false;
	var objInfo = results.objInfo = [];

	if(x<0 || y<0 || w<0 || h<0)
		return results;

	var gui = $('gui');
	var e_gui = $('e_gui');
	if(gui)
		var grid = this.game.grid;
	if(e_gui)
		var grid = window.levelEditor.grid;

	//var gridScale = window.shell.gridScale;

	/*var id = objInfo;
	var objInfo = objInfo.split(",");

	var type = objInfo[0];*/
	/*var left = parseInt(objInfo[1])/gridScale;
	var top = parseInt(objInfo[2])/gridScale;
	var width = parseInt(objInfo[3])/gridScale;
	var height = parseInt(objInfo[4])/gridScale;*/

	for (var i=x; i<(x+w); i++){
		for (var j=y; j<(y+h); j++){
			var currBlock = grid[i][j];
			 
			if(currBlock != element && currBlock != ""){
				var inArray = false;
				for (var k=0; k<objInfo.length; k++){
					if(objInfo[k] == currBlock){
						inArray = true;
					}
				}
				if (!inArray){
					results.collided = true;
					objInfo.push(currBlock);
				}
			}
		}
	}
	return results;
}

/*function checkBounds(key){
	var player = this.player;
	
	switch(key){
		case "W":
			var allow = (player.top <= 0) ? false : true;
			return allow;
			break;
		case "A":
			var allow = (player.left <= 0) ? false : true;
			return allow;
			break;
		case "S":
			var allow = (player.bottom >= 700) ? false : true;
			return allow;
			break;
		case "D":
			var allow = (player.right >= 1100) ? false : true;
			return allow;
			break;
		default:
			break;
	}
}*/

/*function buildMapBoundaries(){
	
	// Grabs the current MapContainer (for undefined purposes (7/31/2012)
	// Grabs the current world element and creates a mapBoundaries (x) and (y) grid using values 0 (empty) and 1 (filled?) to determine collision
	
	var MapContainer = $("MapContainer");
	var world = $("world"),
		worldWidth = parseInt(world.style.width),
		worldHeight = parseInt(world.style.height);
	
	
	var mapBoundaries = this.mapBoundaries;
	for (var i=0; i<worldWidth; i++){
		mapBoundaries["x"][i] = 0;
	}
	for (var i=0; i<worldHeight; i++){
		mapBoundaries["y"][i] = 0;
	}
	
	var worldObjects = world.childNodes;
	for (var i=0; i<worldObjects.length; i++){
		if (worldObjects[i].id == "object"){
			var object = worldObjects[i],
				objLeft = parseInt(object.style.left),
				objTop = parseInt(object.style.top),
				objWidth = parseInt(object.style.width),
				objHeight = parseInt(object.style.height),
				objRight = objLeft + (objWidth-1),
				objBottom = objTop + (objHeight-1);
			
			for (var j=objLeft; j<=objRight; j++){
				mapBoundaries["x"][j] = 1;
			}
			for (var j=objTop; j<=objBottom; j++){
				mapBoundaries["y"][j] = 1;
			}
		}
	}
	//grab each childNode of the "world" div
	//for each child grab their (left (x) + width (x2)) and (top (y) + height (y2)) and add all the values in those ranges...
	//to mapBoundaries(x) and mapBoundaries(y)
	debugger;
}*/
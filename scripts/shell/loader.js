var loader = window.loader = {};
loader.endCount = 0;
loader.currentCount = 0;

loader.files_loading = 0;
loader.files_loaded = 0;

loader.load = function(js, css){
	//The config should be defined in topConfig currently
	var js = js || window.js;
	var css = css || window.css;

	loader.startLoadingProgress(js.length + css.length);
	loader.loadJS(js);
	loader.loadCSS(css);
}
 
loader.loadJS = function(files){
	for (var i=0; i<files.length; i++){
		var js = document.createElement("script");
		js.src = files[i];
		js.onload = function() {
    		loader.load_progress();
    		loader.updateLoadingProgress();
    	}

		document.head.appendChild(js);
		loader.files_loading++;
	}
}

loader.loadCSS = function(files){
	for (var i=0; i<files.length; i++){
		var css = document.createElement("link");
		css.rel = "stylesheet";
		css.type = "text/css";
		css.href = files[i];
		css.onload = function() {
    		loader.load_progress();
    		loader.updateLoadingProgress();
    	}

		document.head.appendChild(css);
		loader.files_loading++;
	}
}

//This is used to specifically track when to call finishLoading()
loader.load_progress = function(){
	loader.files_loaded++;
	if (loader.files_loaded == loader.files_loading){
		loader.finishLoading();
	}
}

loader.finishLoading = function(){
	var screenId = window.screenId || null;
	callback_finishLoading();
}

/*loader.setupLoadingScreenDisplay = function(element){
	var element = $(element);
	fitToContainerElement(element);
}*/

//Trigger the loading_screen to be visible and start tracking load progress
loader.startLoadingProgress = function(count){
	loader.endCount = count;
	//this.setupLoadingScreenDisplay("loading_screen");
	setVisibility("loading_screen",true);
}

//Update the loading_screen and hide it once we've finished loading
loader.updateLoadingProgress = function(){
	var loading_screen = $('loading_screen');
	if (!loading_screen){
		return;
	}

	var loadingBar = $('loadingBar');
	var progress = $('progressBar');
	var progressText = $('progressText');
	
	var ec = this.endCount;
	this.currentCount++;
	var cc = this.currentCount;
	var percent = cc / ec;
	var percentString = (percent.toString().substring(2,4) || 0) + "%";
	
	//loading_screen.style.visibility = percent != 1 ? "visible" : "hidden";
	//var barWidth = Math.floor(parseInt(loadingBar.style.width) * percent);
	//var barText = percent.toString().substring(2,4) || 0;
	progressBar.style.width = percentString;//px(barWidth);
	progressText.innerHTML = percentString;//barText+"%"; //innerText

	if (cc == ec){
		this.currentCount = 0;
		setVisibility("loading_screen",false);
	}
}
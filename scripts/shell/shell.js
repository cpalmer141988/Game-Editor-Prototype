top.gridScale = shell.gridScale = 16; 
// Move this to grid, why should shell care about the grid scale?

shell.setSize = function(){
	var windowSize = getWindowSize();

	var body = $('levelEditor');
	body.style.width = px(windowSize[0]);
	body.style.height = px(windowSize[1]);

	// Move this elsewhere, into a config file maybe?
	var elementsArray = ['shell', 'e_gui', 'e_menuBar', 'e_console', 'e_toolBar', 'e_grid', 'toolInterfaceContainer'];

	for (var i=0; i<elementsArray.length; i++) {
		var elem = elementsArray[i];
		if ($(elem)) {
			switch (elem) {
				case "shell":
					var shell_w = parseInt(getStyle('shell',"width"));
					var shell_h = parseInt(getStyle('shell',"height"));
					break;
				case "e_gui":
					e_gui = $(elem);
					e_gui.style.width = px(shell_w);
					e_gui.style.height = px(shell_h);
					break;
				case "e_menuBar":
					var e_menuBar_h = parseInt(getStyle('e_menuBar',"height"));
					e_menuBar = $(elem);
					break;
				case "e_console":
					var e_console_h = parseInt(getStyle('e_console',"height"));
					break;
				case "e_toolBar":
					var e_toolBar_w = parseInt(getStyle('e_toolBar',"width"));
					e_toolBar = $(elem);
					e_toolBar.style.height = px(shell_h - e_menuBar_h - e_console_h);
					e_toolBar.style.top = px(e_menuBar_h);
					break;
				case "e_grid":
					e_grid = $(elem);
					e_grid.style.width = px(shell_w - e_toolBar_w);
					e_grid.style.height = px(shell_h - e_menuBar_h - e_console_h);
					e_grid.style.left = px(e_toolBar_w);
					e_grid.style.top = px(e_menuBar_h);
					break;
				case "toolInterfaceContainer":
					toolInterfaceContainer = $(elem);
					toolInterfaceContainer.style.width = px(shell_w - e_toolBar_w);
					toolInterfaceContainer.style.height = px(shell_h - e_menuBar_h - e_console_h);
					toolInterfaceContainer.style.left = px(e_toolBar_w);
					toolInterfaceContainer.style.top = px(e_menuBar_h);
					break;
			}
		}
	}

	// OLD VERSION I WANT TO REPLACE WITH SOMETHING MORE DYNAMIC
	/*var shell = $('shell');

	var gui = $('gui');
	var e_gui = $('e_gui');

	var game = $('game');
	var e_grid = $('e_grid');
	var toolInterfaceContainer = $('toolInterfaceContainer');

	//100px are reserved for the border area of the shell
	if (shell){
		shell.style.width = 1280 * scale + "px";
		shell.style.height = 720 * scale + "px";
	}
	if (e_gui){
		e_gui.style.width = 1280 * scale - 100 + "px";
		e_gui.style.height = 720 * scale - 100 + "px";
	}
	if (e_grid){
		e_grid.style.width = 1280 * scale - 100 + "px";
		e_grid.style.height = 720 * scale - 100 + "px";
	}
	if (toolInterfaceContainer){
		toolInterfaceContainer.style.width = 1280 * scale - 100 + "px";
		toolInterfaceContainer.style.height = 720 * scale - 100 + "px";
	}*/

}

shell.showTooltip = function(event){

}

/*shell.setSize = function(){
	var shell = $("shell");
	var header = $("header");
	var navigation = $("navigation");
	var account = $("account");
	var footer = $("footer");

	var size = getWindowSize();
	var w = size[0];
	var h = size[1];
	//var accountWidth = parseInt(account.style.width);
	//var footerTop = h - parseInt(footer.style.height);
	
	shell.style.width = w < 950 ? "950px" : w+"px";
	shell.style.height = h < 550 ? "550px" : h+"px";
	header.style.width = w < 950 ? "950px" : (w-accountWidth)+"px";
	navigation.style.width = w < 950 ? "950px" : (w-accountWidth)+"px";
	account.style.left = (w-accountWidth)+"px";
	footer.style.width = w < 950 ? "950px" : w+"px";
	footer.style.top = footerTop+"px";

	shell.style.visibility = "visible";
}*/

shell.getXMLHttp = function (){
	var xmlhttp;
	if (window.XMLHttpRequest){			// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {							// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}
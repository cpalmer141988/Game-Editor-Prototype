function startInput(event){
	var key = event.keyCode;//String.fromCharCode(event.keyCode); //turning keyCodes into characters doesn't work for keys such as spacebar, enter, and delete

	var gui = $('gui');
	var e_gui = $('e_gui');
	if (gui){
		switch(key){
			case "W": //W
			case "A": //A
			case "S": //S
			case "D": //D
				var currCommands = this.player.currCommands;
				var existance=0;
				for (var i=0; i<currCommands.length; i++){
					if (currCommands[i] == key) {return;}
					else {existance++;}
				}
				if (existance == currCommands.length){
					currCommands.push(key);
				}
				break;

			case 16: //shift
				break;
			case 17: //ctrl
				//event.preventDefault(); //need to override ctrl+shift+j somehow, ctril alone doesn't prevent the debugger from opening
				break;
			case 18: //alt
				break;
			case 32: //space
				break;
			case 13: //enter
				break;
			case 8:  //backspace
				break;
			case 46: //delete
				break;
			case 123: //F12
				//event.preventDefault();
				break;

			case "q": //q
				break;
			case "e": //e
				break;
			case "r": //r
				break;
			case "f": //f
				break;

			case "0": //0
				break;
			case "1": //1
				break;
			case "2": //2
				break;
			case "3": //3
				break;
			case "4": //4
				break;
			case "5": //5
				break;
			case "6": //6
				break;

			default:
				break;
		}
	}
	if (e_gui){
		switch(key){
			case "W": //W
			case "A": //A
			case "S": //S
			case "D": //D
				/*var currCommands = this.player.currCommands;
				var existance=0;
				for (var i=0; i<currCommands.length; i++){
					if (currCommands[i] == key) {return;}
					else {existance++;}
				}
				if (existance == currCommands.length){
					currCommands.push(key);
				}*/
				break;

			case 16: //shift
				break;
			case 17: //ctrl
				//event.preventDefault(); //need to override ctrl+shift+j somehow, ctril alone doesn't prevent the debugger from opening
				break;
			case 18: //alt
				break;
			case 32: //space
				break;
			case 13: //enter
				var levelEditor = top.levelEditor;
				var block = levelEditor.selectedBlock;
				if(block != ""){
					block = $(block);
					levelEditor.highlight_Block(block.id, false);
					levelEditor.selectedBlock = "";
				}
				break;
			case 8: //backspace
				//event.preventDefault();
				break;
			case 46: //delete
				event.preventDefault();
				var levelEditor = top.levelEditor;
				var block = levelEditor.selectedBlock;
				if(block != ""){
					block = $(block);
					grid.removeObj(block.id);
					block.parentNode.removeChild(block);
					levelEditor.selectedBlock = "";
				}
				break;
			case 123: //F12
				//event.preventDefault();
				break;

			case "q": //q
				break;
			case "e": //e
				break;
			case "r": //r
				break;
			case "f": //f
				break;

			case "0": //0
				break;
			case "1": //1
				break;
			case "2": //2
				break;
			case "3": //3
				break;
			case "4": //4
				break;
			case "5": //5
				break;
			case "6": //6
				break;

			default:
				break;
		}
	}
}

function stopInput(event){
	var key = event.keyCode;//String.fromCharCode(event.keyCode);  //turning keyCodes into characters doesn't work for keys such as spacebar, enter, and delete
	
	var gui = $('gui');
	var e_gui = $('e_gui');
	if (gui){
		switch(key){
			case "W": //W
			case "A": //A
			case "S": //S
			case "D": //D
				var currCommands = this.player.currCommands;
				for (var i=0; i<currCommands.length; i++){
					if (currCommands[i] == key) { currCommands.splice(i,1); }
				}
				break;

			case 16: //shift
				break;
			case 32: //space
				break;
			case 13: //enter
				break;
			case 8:  //backspace
				break;
			case 46: //delete
				break;

			case "q": //q
				break;
			case "e": //e
				break;
			case "r": //r
				break;
			case "f": //f
				break;

			case "0": //0
				break;
			case "1": //1
				break;
			case "2": //2
				break;
			case "3": //3
				break;
			case "4": //4
				break;
			case "5": //5
				break;
			case "6": //6
				break;

			default:
				break;
		}
	}
	if (e_gui){
		switch(key){
			case "W": //W
			case "A": //A
			case "S": //S
			case "D": //D
				/*var currCommands = this.player.currCommands;
				for (var i=0; i<currCommands.length; i++){
					if (currCommands[i] == key) { currCommands.splice(i,1); }
				}*/
				break;

			case 16: //shift
				break;
			case 32: //space
				break;
			case 13: //enter
				break;
			case 8:  //backspace
				break;
			case 46: //delete
				break;

			case "q": //q
				break;
			case "e": //e
				break;
			case "r": //r
				break;
			case "f": //f
				break;

			case "0": //0
				break;
			case "1": //1
				break;
			case "2": //2
				break;
			case "3": //3
				break;
			case "4": //4
				break;
			case "5": //5
				break;
			case "6": //6
				break;

			default:
				break;
		}
	}
}

function execPlayerCommands(){
	var currCommands = this.player.currCommands;
	for (var i=0; i<currCommands.length; i++){
		var key = currCommands[i];
		switch(key){
			case "W":
			case "A":
			case "S":
			case "D":
				movePlayer(key);
				break;
			default:
				break;
		}
	}

	var t=setTimeout("execPlayerCommands()",10);
}
function inherit(superClass, subClass){
	subClass.prototype = new superClass();
}

function $(element){
	var element = (typeof element === "string") ? document.getElementById(element) : element;
	return element;
}

function px(num){
	return num+"px";
}

function getCssRule(selectorText, path){
	var doc = path || document;
	var stylesheets = doc.styleSheets;

	for (var i=0; i<stylesheets.length; i++){
		var stylesheet = stylesheets[i];
		var rules = stylesheet.rules;
		if (!rules){
			rules = stylesheets.cssRules;
		}

		for (var j=0; j<rules.length; j++){
			var rule = rules[j];
			
			if (rule.selectorText == selectorText){
				return rule;
			}
		}
	}
}

function getStyle(element, cssRule){
	//Simplify the syntax for getting a style from an element
	element = $(element);

	var strValue = "";
	if(document.defaultView && document.defaultView.getComputedStyle){
		strValue = document.defaultView.getComputedStyle(element, "").getPropertyValue(cssRule);
	}
	else if(element.currentStyle){
		cssRule = cssRule.replace(/\-(\w)/g, function (strMatch, p1){
			return p1.toUpperCase();
		});
		strValue = element.currentStyle[cssRule];
	}
	return strValue;
}

function getWindowSize(){
	var size = [0, 0];
	if (typeof window.innerWidth != "undefined") {
		size = [window.innerWidth, window.innerHeight];
	}
	else if (typeof document.documentElement != "undefined" && typeof document.documentElement.clientWidth != "undefined" && document.documentElement.clientWidth != 0) {
		size = [document.documentElement.clientWidth, document.documentElement.clientHeight];
	}
	else {
		size = [document.getElementsByTagName("body")[0].clientWidth, document.getElementsByTagName("body")[0].clientHeight];
	}
	return size;
};

function fitToContainerElement(element){
	var element = $(element);
	//incomplete, obviously!
}

function stopBubbling(event){
	var e = event || window.event;
	e.cancelBubble = true;
	if (e.stopPropagation){
		e.stopPropagation();
	}
}

function isVisible(element){
	var state = false;
	if (typeof element === "string"){
		state = $(element).style.visibility == "visible" ? true : false;
	} else {
		state = element.style.visibility == "visible" ? true : false;
	}
	return state;
}

function setVisibility(element,state){
	var element = (typeof element === "string") ? $(element) : element;
	if (element){
		element.style.visibility = state ? "visible" : "hidden";
	}
}

function setDisplay(element,state){
	var element = (typeof element === "string") ? $(element) : element;
	if (element){
		element.style.display = state ? "block" : "none";
	}
}

function setBColor(element,color){
	var element = (typeof element === "string") ? $(element) : element;
	element.style.backgroundColor = color;
}

function getXMLHttp(){
	var xmlhttp;
	if (window.XMLHttpRequest){			// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {							// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}

var top = window;
var shell = top.shell = {};
var gui = top.gui = {};
var game = top.game = {};
var levelEditor = top.levelEditor = {};

var js = [
			"scripts/shell/timer.js",
			"scripts/shell/shell.js",
			"scripts/shell/grid.js",
			"scripts/shell/collision.js",
			"scripts/shell/keyboard.js",

			"scripts/levelEditor/levelEditor.js",
			"scripts/levelEditor/menuBar.js",
			"scripts/levelEditor/toolBar.js",
			"scripts/levelEditor/console.js"
		 ];

var css = []; // Replaced this with SASS, no longer needed.

function selectSpawnPoint(){
	var spawnPoint = {};
	var x = Math.floor((Math.random()*2)+1);
	var y = Math.floor((Math.random()*2)+1);
	
	spawnPoint.left = (x==1) ? (Math.floor((Math.random()*30)+1) - 50) : (Math.floor((Math.random()*30)+1) + 400);
	spawnPoint.top = (y==1) ? (Math.floor((Math.random()*30)+1) - 50) : (Math.floor((Math.random()*30)+1) + 400);
	
	return spawnPoint;
}

function spawningEffect(){
	var spawningEnemies = this.spawningEnemies;
	
	for (var i=0; i<spawningEnemies.length; i++){
		var enemy = spawningEnemies[i];
		
		switch(enemy.spawnTimer){
			case 0:
				startEnemyAI();
				break;
			case 1:
			case 3:
			case 5:
			case 7:
			case 9:
				$(enemy.id).style.backgroundColor = "red";
				var t=setTimeout("spawningEffect()",100);
				break;
			case 2:
			case 4:
			case 6:
			case 8:
			case 10:
				$(enemy.id).style.backgroundColor = "orange";
				var t=setTimeout("spawningEffect()",100);
				break;
			default:
				break;
		}
		enemy.spawnTimer--;
	}
}
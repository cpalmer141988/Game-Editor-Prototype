function buildEnemy(){
	var enemies = this.enemies;
	var spawningEnemies = this.spawningEnemies;
	var currEnemy = enemies.length;
	
	var enemy = {};
	enemy.id = "enemy"+currEnemy;
	enemy.isSpawning = true;
	enemy.spawnTimer = 10;
	enemies.push(enemy);
	spawningEnemies.push(enemy);
	
	var spawnPoint = selectSpawnPoint();
	var enemyHTML = document.createElement('div');
	enemyHTML.setAttribute("id",enemy.id);
	enemyHTML.style.backgroundColor = "orange";
	enemyHTML.style.width = 20+"px";
	enemyHTML.style.height = 20+"px";
	enemyHTML.style.left = spawnPoint.left+"px";
	enemyHTML.style.top = spawnPoint.top+"px";
	$('world').appendChild(enemyHTML);
	
	spawningEffect();
	//var t=setTimeout("buildEnemy()",5000);
}
function buildPlayer(){
	var player = {};
	player.left = parseInt($("player").style.left);
	player.right = player.left+20;
	player.top = parseInt($("player").style.top);
	player.bottom = player.top+20;
	player.currCommands = [];
	
	//Health = bar of health (damage that can be taken before using reserves to regenerate)
	//Lives = critical/fatal wounds pull from "spirit/chi reserves"

	//Makes player seem "immortal" but to an extent. No extra lives, live are continuous from regenerating using spirit/chi
	//when spirit/chi is gone, fatal damage will result in game over.

	//Mental states / characteristics are built/developed by choices made by the player.
	//Lots of ego could result in "cutscenes" where the character takes damage to show off and runs into reserves (making the game harder) 

	this.player = player;
	execPlayerCommands();
}

/*player.move = function move(){

}*/

function movePlayer(key){
	var left = this.player.left;
	var right = this.player.right;
	var top = this.player.top;
	var bottom = this.player.bottom;

	switch(key){
		case "W":
			if (!checkBounds(key)) {return;}
			var player = $("player").style;
			var position = parseInt(player.top);
			this.player.top = top - 1;
			this.player.bottom = bottom - 1;
			player.top = (position - 1) + "px";
			break;
		case "A":
			if (!checkBounds(key)) {return;}
			var player = $("player").style;
			var position = parseInt(player.left);
			this.player.left = left - 1;
			this.player.right = right - 1;
			player.left = (position - 1) + "px";
			break;
		case "S": 
			if (!checkBounds(key)) {return;}
			var player = $("player").style;
			var position = parseInt(player.top);
			this.player.top = top + 1;
			this.player.bottom = bottom + 1;
			player.top = (position + 1) + "px";
			break;
		case "D":
			if (!checkBounds(key)) {return;}
			var player = $("player").style;
			var position = parseInt(player.left);
			this.player.left = left + 1;
			this.player.right = right + 1;
			player.left = (position + 1) + "px";
			break;
		default:
			break;
	}
}
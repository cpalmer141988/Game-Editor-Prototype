//Use this to create blocks to fill out the map with.

function levelGen(){}

levelGen.genSky = function(x, y){
	var sky = $('sky');
}

levelGen.genWorld = function(x, y){
	var world = $('world');
	var gridScale = shell.gridScale;
	var x = x / gridScale;
	var y = y / gridScale;

	for (var i=0; i<x; i++){
		var xBlock = document.createElement("div");
		xBlock.style.width = gridScale + "px";
		xBlock.style.height = gridScale + "px";
		xBlock.style.left = i * gridScale + "px";
		xBlock.style.top = "0px";

		if (i % 2 == 0)
			xBlock.style.backgroundColor = "grey";
		else
			xBlock.style.backgroundColor = "white";

		world.appendChild(xBlock);

		for (var j=1; j<y; j++){
			var yBlock = document.createElement("div");
			yBlock.style.width = gridScale + "px";
			yBlock.style.height = gridScale + "px";
			yBlock.style.left = i * gridScale + "px";
			yBlock.style.top = j * gridScale + "px";
 
			if ((i+j) % 2 == 0)
				yBlock.style.backgroundColor = "grey";
			else
				yBlock.style.backgroundColor = "white";

			world.appendChild(yBlock);
		}
	}
}